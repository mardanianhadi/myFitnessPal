export default {
    information: {
        continue: 100,
        switchingProtocol: 101,
        processing: 102
    },
    success: {
        ok: 200,
        created: 201,
        accepted: 202,
        noContent: 204
    },
    redirection: {
        found: 302
    },
    clientError: {
        badRequest: 400,
        unauthorized: 401,
        paymentRequired: 402,
        forbiden: 403,
        notFound: 404,
        methodNotAllowed: 405,
        gone: 410,
        tooManyRequests: 429
    },
    serverError: {
        internalError: 500,
        notImplemented: 501,
        badGetway: 502,
        serviceUnavailable: 503
    }
}