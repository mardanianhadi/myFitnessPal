import { mongoose } from '../modules/index.js';
import { bcrypt } from '../modules/index.js';

const Schema = new mongoose.Schema({
    email: { type: String, required: true },
    password: { type: String, required: true },
    image: { type: String, default: '' },
    phone: { type: String, default: '' },
    firstname: { type: String, default: '' },
    lastname: { type: String, default: '' },
});

Schema.pre('save', function (next) {
    let password = this.password;
    let salt = bcrypt.genSaltSync(15);
    let hash = bcrypt.hashSync(password, salt);
    this.password = hash;
    next();
});

Schema.pre('updateOne', function (next) {
    let update = this.getUpdate()
    let password = update.password;
    if(password){
        let salt = bcrypt.genSaltSync(15);
        let hash = bcrypt.hashSync(password, salt);
        this.getUpdate().password = hash;
    }
    next();
})

Schema.methods.comparePassword = function (password) {
    return bcrypt.compareSync(password, this.password)
}


export default mongoose.model('User', Schema);