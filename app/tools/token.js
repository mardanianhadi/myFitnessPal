import { jwt, autoBind, config } from '../modules/index.js';


export default new class {
    constructor() {
        autoBind(this);

        this.secret = config.get('jwt.secret');
    }

    async create(payload) {
        let self = this;
        return jwt.sign(payload, self.secret);
    }
    async verify(token) {
        try {
            let self = this;
            return jwt.verify(token, self.secret);
            
        } catch (ex) {
            console.log(ex);
            return null;
        }
    }
}