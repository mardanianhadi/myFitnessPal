import mongoose from 'mongoose';
const ObjectId = mongoose.Types.ObjectId;

const schema = new mongoose.Schema({
    user: { type: ObjectId, ref: 'User', required: true },
    food: [{ type: ObjectId, ref: 'Food', required: true }]
});

export default mongoose.model('Favourite', schema);