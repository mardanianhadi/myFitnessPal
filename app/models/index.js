import User from './user.js';
import Food from './food.js';
import Favourite from './favourite.js';
import Category from './category.js';

export {
    User,
    Food,
    Favourite,
    Category
}