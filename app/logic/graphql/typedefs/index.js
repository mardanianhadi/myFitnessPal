import { gql } from 'apollo-server-express';

const typeDefs = gql(`    
    
    type User {
        password: String
        firstname: String
        lastname: String
        image: String
        email: String
        phone: String
        
        _id: String
    }
    type Food {
        title: String
        image: String
        _id: String
        category: String
        meal: String
        materials: String
        recipes: String
        protein: String
        fat: String
        carbs: String
        favorite: Boolean
    }
    
    type Error { 
        message: String
        code: Int
    }
    type Category {
        title: String
        _id: String
    }
    type AuthRes { 
        status: Int!
        error: Error
        token: String
        message: String
    }
    type Response {
        status: Int
        error: Error
        message: String
    }
    type ListFood {
        status: Int
        message: String
        error: Error
        data: [Food]
    }
    type UserResponse { 
        status: Int
        message: String
        error: Error
        data: User
    }
    type ListUser {
        status: Int
        message: String
        error: Error
        data: [User]
    }
    type ListCategory {
        status: Int
        message: String
        error: Error
        data: [Category]
    }
    type Favourite {
        _id: String
        user: [User]
        food: [Food]
    }
    
    type ListFavourite {
        status: Int
        message: String
        data: [Food]
    }

    type Mutation {
        login(email: String!, password: String!): AuthRes
        register(email: String!, password: String!): AuthRes

        addFood(
            meal: String!
            materials: String!
            recipes: String!,
            title: String!,
            image: String!, 
            token: String!,
            carbs: String!,
            fat: String!,
            protein: String!,
            category: String!): Response
            editFood(
                foodId: String!,
                meal: String!
                materials: String!
                recipes: String!,
                title: String!,
                image: String!, 
                token: String!,
                carbs: String!,
                fat: String!,
                protein: String!,
                category: String!): Response
        deleteFood(id: String!,token: String!): Response
        
        addFavourite(id: String!, token: String!, favorite: Boolean!): Response
        deleteFavourite(id: String!, token: String!): Response
            
        addCategory(title: String!,token: String!): Response
        deleteCategory(id: String!,token: String!): Response

        editProfile(
            email: String, 
            phone: String, 
            password: String, 
            image: String, 
            firstname: String, 
            lastname: String,
            token: String!
        ): Response
    }

    type Query {
        listFood(token: String!): ListFood
        checkToken(token: String!): AuthRes
        getProfile(token: String!): UserResponse
        listUser(token: String!): ListUser
        listCategory(token: String!): ListCategory
        listFavourite(token: String): ListFavourite
    }    
`);



export default typeDefs;