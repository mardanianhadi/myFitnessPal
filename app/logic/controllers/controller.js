import { autoBind } from '../../modules/index.js';

export default class {
    constructor() {
        autoBind(this);
    }
}