import Controller from '../controller.js';
import { } from '../../../modules/index.js';
import { token as tokenTools } from '../../../tools/index.js';
import { User } from '../../../models/index.js';




export default new class extends Controller {
    constructor() {
        super();
    }
    async login(data, context) {
        try {
            let { email, password } = data;
            let now = Date.now();
            
            let user = await User.findOne({email});
            if (!user) {
                return {
                    message: 'not fount',
                    status: 1
                }
            }
            if (!user.comparePassword(password)) {
                return {
                    message: 'invalid password',
                    status: 2
                }
            }
            let token = await tokenTools.create({ id: user._id, expire: 1000 * 60 * 60 * 24 * 365 * 10 });
            return {
                message: 'login success',
                status: 0,
                token
            }
        } catch (ex) {
            console.log(ex);
            return {
                message: 'error',
                status: -1
            }
        }
    }
    async checkToken(data, context) {
        try {
            let { token } = context;
            let { id: _id, expire } = await tokenTools.verify(token, token);

            let now = Date.now();
            if(expire > now){
                return {
                    status: 1,
                    message: 'expire'
                }
            }
            let user = await User.findById(_id);
            if(!user){
                return {
                    status: 2,
                    message: 'not found'
                }
            }
            return {
                status: 0,
                message: 'valid'
            }
        } catch (ex) {
            console.log(ex);
            return {
                message: 'error',
                status: -1
            }
        }
    }
}