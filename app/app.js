import {
    bodyParser,
    cookieParser,
    express,
    http,
    path,
    mongoose,
    chalk,
    cors,
    Sequelize,
    ApolloServer,
    autoBind,
    passport,
    session,
    config,
    dotenv
} from './modules/index.js';

import resolvers from './logic/graphql/resolvers/index.js';
import typedefs from './logic/graphql/typedefs/index.js';
import router from './routes/index.js';
import { User } from './models/index.js';



export default class {

    #app
    #server
    constructor() {
        autoBind(this);
        this.#app = express();
        this.server();
        this.db();
        this.setup();
        this.routes();
    }

    server() {
        let self = this;
        dotenv.config({
            path: path.resolve('./config/.env')
        })
        const PORT = process.env.PORT;
        this.#server = http.createServer(this.#app);
        console.log(';;;;;;;;;;;;;;;;;;;;;', process.env.NODE_ENV);
        const GraphqlServer = new ApolloServer({
            typeDefs: typedefs,
            resolvers,
            plugins: [
                // ApolloServerPluginDrainHttpServer({ httpServer: self.#app })
                // ApolloServerPluginLandingPageLocalDefault()
            ],
            introspection: true,
            playground: true,
            context: function ({ req, res }) {
                return { ...req.headers, token: req?.body?.variables?.token };
            }

        })

        GraphqlServer.start().then(() => {
            console.log("Apollo sever is running")
            self.#app.use(express.json({ limit: '1000mb' }));
            self.#app.use(express.urlencoded({ limit: 1024 * 1024 * 1024 * 20 }));
            self.#app.use('/static', express.static(path.resolve('./public')));
            GraphqlServer.applyMiddleware({ app: self.#app, path: '/graphql' });
            self.#app.listen(80, console.log.bind(null, 'Server is running on port ' + 80));

        });

    }
    db() {
        const { CONNECTION_STRING: uri } = process.env;
        mongoose.connect(uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
            .then(() => console.log(chalk.greenBright(`connected to mongodb`)))
            .catch((ex) => console.log(chalk.redBright('mongodb connection error', ex)))
    }
    db2() {
        const { CONNECTION_STRING: uri } = process.env;
        let seq = new Sequelize(uri, {
            username: config.get('mysql.username'),
            password: config.get('mysql.password')
        })
        seq.validate().then(() => console.log('connected to mysql'))
    }
    setup() {
        //setting

        //middleware

        this.#app.use(bodyParser.json({ limit: 1024 * 1024 * 1024 * 20 }));
        this.#app.use(bodyParser.urlencoded({ limit: 1024 * 1024 * 1024 * 20 }))
        this.#app.use(cookieParser(process.env.COOKIE_SECRET));
        this.#app.use(cors({
            allowedHeaders: '*'

        }))
        this.#app.use(session({
            secret: process.env.SECRET,
            cookie: { signed: true },
            name: "sessionid",
            resave: true,
            saveUninitialized: true
        }));

        passport.serializeUser(function (user, done) {
            done(null, user.id);
        });
        passport.deserializeUser(function (id, done) {
            User.findById(id, function (err, user) {
                done(err, user);
            });
        });
        passport.initialize();
        passport.session();

    }
    routes() {


        this.#app.use((req, res, next) => {
            res.setHeader('Content-Type', 'application/json');
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Methods', '*');
            res.setHeader('Access-Control-Allow-Headers', '*');
            next();
        });
        this.#app.use('/api', router);

        // this.#app.use((req, res) => {
        //     res.json({
        //         error: {
        //             message: 'not found',
        //             status: 404
        //         },
        //         data: null
        //     })
        // })
    }
}