import mongoose from 'mongoose';
const ObjectId = mongoose.Types.ObjectId;

const schema = new mongoose.Schema({
    title: { type: String, required: true },
    image: { type: String, required: true },
    category: {type: ObjectId, ref: 'Category', default: null},
    meal: {type: String, required: true},
    materials: {type: String, required: true},
    recipes: {type: String, required: true},
    protein: {type: String, required: true},
    fat:{type: String, required: true},
    carbs:{type: String, required: true},
    favorite: {type: Boolean, default: false}
});

export default mongoose.model('Food', schema);